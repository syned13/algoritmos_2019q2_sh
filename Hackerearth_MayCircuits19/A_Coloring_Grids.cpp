#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <queue>

using namespace std;

typedef unsigned long long ll;

int main() {

    ll n, k;
    cin >> n >> k;

    if(n == 2)
        cout << k*k << endl;
    else if( n == 1)
        cout << 0 << endl;
    else
        cout << k << endl;

    return 0;


}
