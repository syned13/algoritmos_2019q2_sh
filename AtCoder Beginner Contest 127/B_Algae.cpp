#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <queue>

using namespace std;

typedef unsigned long long ll;


int main() {

    int r, D, x2;
    cin >> r >> D >> x2;

    vector<int> arr (11);
    arr[0] = x2;

    for(int i = 1; i < 11; i++)
    {
        arr[i] = r*arr[i-1] - D;
        cout << arr[i] << endl;
    }

}
