#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <queue>

using namespace std;

typedef unsigned long long ll;


int main() {

    int a, b;
    cin >> a >> b;

    if(a >= 13)
        cout << b;
    else if(a >= 6 && a <= 12)
        cout << b/2;
    else
        cout << 0;


    return 0;

}
