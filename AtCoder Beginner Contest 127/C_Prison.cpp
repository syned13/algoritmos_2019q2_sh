#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <queue>

using namespace std;

typedef unsigned long long ll;


int main() {

   int n, m;
   cin >> n >> m;

   vector<pair<int,int>> ids(m);

   int counter = 0;

   pair<int,int> minFirst;
   minFirst.first = 10e9;

   pair<int,int> maxFirst;
   maxFirst.first = 0;

   pair<int,int> maxSecond;
   maxSecond.second = 0;
   pair<int,int> minSecond;
   minSecond.second = 10e9;

   for(int i = 0; i < m; i++)
   {
       pair<int,int> temp;
       cin >> temp.first;
       cin >> temp.second;

       if(temp.first < minFirst.first)
           minFirst = temp;
       if(temp.first > maxFirst.first)
           maxFirst = temp;
       if(temp.second < minSecond.second)
           minSecond = temp;
       if(temp.second > maxSecond.second)
           maxSecond = temp;

       ids[i] = temp;

   }


   for(int i = 1; i <= n; i++)
   {
       bool all = true;

       if(i >= minFirst.first && i <= minFirst.second && i >= maxFirst.first && i <= maxFirst.second && i >= minSecond.first && i <= minSecond.second
       && i >= maxSecond.first && i <= maxSecond.second)
       {
           counter++;
       }


   }

   cout << counter;
   return 0;

}
