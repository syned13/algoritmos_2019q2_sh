class Solution {
public:
    int twoSumLessThanK(vector<int>& A, int K) {

        int maxSum = -1;
        for(int i = 0; i < A.size(); i++)
        {
            for(int j = i+1; j < A.size(); j++)
            {
              if(A[i] + A[j] > maxSum && A[i] + A[j] < K)
                  maxSum = A[i] + A[j];
            }
        }
        return maxSum;

    }
};
