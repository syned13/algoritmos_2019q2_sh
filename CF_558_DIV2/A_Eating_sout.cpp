#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>


using namespace std;

typedef unsigned long long ll;


int main() {

    int n, m;
    cin >> n >> m;

    if(m == 0 || m == 1)
        cout << 1;
    else if(n == m)
        cout << 0;
    else if(n % m == 0)
        cout << m;
    else
    {
        int mayor = 1;
        for(int i = 1; i <= m-1; i++)
        {
            if(m + i == n)
                break;
            mayor++;
        }
        cout << mayor;
    }

    return 0;
}
