#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <queue>

using namespace std;

typedef unsigned long long ll;

int main() {

    int n;
    cin >> n;
    vector<int> numbers(2*n);

    for(int i = 0; i < 2*n; i++)
        cin >> numbers[i];

    sort(numbers.begin(), numbers.end());

    int sumFirst = 0;

    for(int i = 0; i < n; i++)
        sumFirst += numbers[i];

    int sumSecond = 0;
    for(int i = n; i < 2*n; i++)
        sumSecond += numbers[i];

    if(sumFirst != sumSecond)
        for(int i = 0; i < 2*n; i++)
            cout << numbers[i] << " ";
    else
        cout << -1;



    return 0;


}
