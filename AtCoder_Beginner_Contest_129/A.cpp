#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>

using namespace std;

typedef unsigned long long ll;


int main() {

    vector<int> numbers (3);

    for(int i = 0; i < 3; i++)
        cin >> numbers[i];

    sort(numbers.begin(), numbers.end());

    cout << numbers[0]+numbers[1];

    return 0;

}
