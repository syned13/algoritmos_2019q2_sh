#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>

using namespace std;

typedef unsigned long long ll;


int main() {

    int n;
    cin >> n;
    vector<int> numbers (n);

    for(int i = 0; i < n; i++)
        cin >> numbers[i];


    vector<int> prefix_sum (n);
    prefix_sum[0] = numbers[0];

    for(int i = 1; i < n; i++)
    {
        prefix_sum[i] = prefix_sum[i-1] + numbers[i];

    }


    int minDifference = 10e9;

    for(int t = 0; t < n-1; t++)
    {
        int diff = abs(prefix_sum[t] - (prefix_sum[n-1] - prefix_sum[t]));
        //cout << diff << endl;
        if (diff < minDifference)
            minDifference = diff;
    }

    cout << minDifference;
    return 0;


}
