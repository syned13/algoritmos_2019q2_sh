#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <map>
#include <list>
#include <set>

using namespace std;

typedef long long ll;

int main() {

    int n, a ,b;
    cin >> n >> a >> b;

    cout << (int)min(n*a,b);
    return 0;
}
