#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <map>
#include <list>
#include <set>

using namespace std;

typedef long long ll;

bool calcDistance(vector<int> p1, vector<int> p2)
{
    double suma = 0;
    for(int i = 0; i < p1.size(); i++)
    {
        suma += pow(p1[i]-p2[i],2);
    }

    suma = sqrt(suma);
    //cout << suma << endl;
    if(floor(suma) == ceil(suma))
        return true;
    return false;
}
int main() {

    int n, d;
    cin >> n >> d;
    vector<vector<int>> points (n);
    for(int i = 0; i < n; i++)
    {
       for(int j = 0; j < d; j++)
       {
           int temp;
           cin >> temp;
           points[i].push_back(temp);
       }
    }

    int counter = 0;

    for(int i = 0; i < n;i++)
    {
        for(int j = i+1; j < n; j++)
        {
            if(j != i)
            {
                if(calcDistance(points[i],points[j]))
                    counter++;
            }
        }

    }

    cout << counter;
    return 0;
}
