#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <map>
#include <list>

using namespace std;

typedef long long ll;

int main() {

    int n;
    cin >> n;

    string sNumber = to_string(n);

    while(true)
    {
        int suma = 0;

        for(auto i : sNumber)
        {
            int temp = i - '0';
            suma += temp;
        }

        if(suma % 4 == 0)
            break;

        n++;
        sNumber = to_string(n);
    }

    cout << sNumber;

    return 0;

}
