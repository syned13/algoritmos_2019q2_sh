#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <queue>

using namespace std;

typedef unsigned long long ll;



int main() {

    ll queries;
    cin >> queries;



    for(ll i = 0; i < queries; i++)
    {
        ll n, k;
        cin >> n >> k;

        ll counter = 0;


        while(n > 0)
        {

            counter += 1 + n%k;
            n /= k;

        }

        cout << counter-1 << endl;

    }


    return 0;


}
