#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <map>
#include <list>
#include <set>

using namespace std;

typedef long long ll;

int main() {

    int n;
    cin >> n;

    vector<int> numbers(n);

    unordered_map<int,int> freq;

    int mayor = -1;
    for(int i = 0; i < n; i++)
    {
        cin >> numbers[i];
        freq[numbers[i]]++;

        if(numbers[i] > mayor)
            mayor = numbers[i];
    }

    vector<int> prefix_sum (mayor+1);
    prefix_sum[0] = freq[0];

    for(int i = 1; i < prefix_sum.size(); i++)
    {
        prefix_sum[i] = freq[i] + prefix_sum[i-1];
    }

    int counter = 0;
    for(int k = 1; k <= mayor; k++)
    {
        if(prefix_sum[k-1] == n/2 )
            counter++;
    }

    cout << counter;

    return 0;
}
