#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <map>
#include <list>

using namespace std;

typedef long long ll;

int main() {

    string s;
    cin >> s;

    unordered_set<char> set;

    for(int i = 0; i < s.size(); i++)
    {
        set.insert(s[i]);
    }

    if(set.size() == 2)
        cout << "Yes";
    else
        cout << "No";

    return 0;

}
