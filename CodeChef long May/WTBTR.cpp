#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>


using namespace std;

typedef unsigned long long ll;

struct Point
{
    int x,y;

};


struct Line
{
    int A, B, C;
    Point point;
    double distanceFromOrigin;
};



bool lineSorter(Line l, Line r)
{
    return l.distanceFromOrigin < r.distanceFromOrigin;
}


double getMinDistance(vector<Point> points)
{
    double minDistance = 10e9;

    vector<Line> lines (points.size());


    for(int i = 0; i < lines.size(); i++)
    {
        Line tempLine;
        tempLine.point = points[i];
        tempLine.C = points[i].x - points[i].y;
        tempLine.A = 1;
        tempLine.B = -1;
        tempLine.distanceFromOrigin =  -points[i].y + points[i].x;
        //tempLine.distanceFromOrigin = abs(tempLine.C) / (sqrt(pow(tempLine.A,2)) + sqrt(pow(tempLine.B,2) ) ); // Porque X y Y siempre se van a cancelar, es solo C

        lines[i] = tempLine;
    }

    sort(lines.begin(), lines.end(), lineSorter);


    for(int i = 0; i < lines.size() - 1; i++)
    {

        if(lines[i].C != lines[i+1].C && lines[i].distanceFromOrigin != lines[i+1].distanceFromOrigin )
        {
            Point p = lines[i].point;
            double distance =  abs(lines[i+1].A*(p.y) + lines[i+1].B*(p.x) + lines[i+1].C) / (sqrt(pow(lines[i+1].A,2) + pow(lines[i+1].B,2) ));

            if(distance < minDistance)
                minDistance = distance;
        }
        else
            return 0;


    }



    vector<Line> lines2 (points.size());


    for(int i = 0; i < lines.size(); i++)
    {
        Line tempLine;
        tempLine.point = points[i];
        tempLine.C = -points[i].x - points[i].y;
        tempLine.A = 1;
        tempLine.B = 1;
        tempLine.distanceFromOrigin =   points[i].y + points[i].x  ;
        //tempLine.distanceFromOrigin = tempLine.C / (sqrt(pow(tempLine.A,2)) + sqrt(pow(tempLine.B,2) ) ); // Porque X y Y siempre se van a cancelar, es solo C

        lines2[i] = tempLine;
    }

    sort(lines2.begin(), lines2.end(), lineSorter);


    for(int i = 0; i < lines2.size() - 1; i++)
    {
        if(lines2[i].C != lines2[i+ 1].C && lines2[i].distanceFromOrigin != lines2[i+ 1].distanceFromOrigin)
        {
            Point p = lines2[i].point;
            double distance =  abs(lines2[i+1].A*(p.y) + lines2[i+1].B*(p.x) + lines2[i+1].C) / (sqrt(pow(lines2[i+1].A,2) + pow(lines2[i+1].B,2) ));

            if(distance < minDistance)
                minDistance = distance;
        }
        else
            return 0;

    }



    return minDistance*sqrt(2) / 2;

}


int main() {

    int queries;
    cin >> queries;

    for(int i = 0; i < queries; i++)
    {
        int n;
        cin >> n;
        vector<Point> points (n);
        for(int j = 0; j < n; j++)
        {
            Point point;
            cin >> point.x >> point.y;
            points[j] = point;
        }


        cout << (double)getMinDistance(points) << endl;
    }



    return 0;
}
