#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>

using namespace std;

typedef unsigned long long ll;
int main() {

    ll queries;
    cin >> queries;
    vector<ll> arr(1000000);

    arr[0] = 1;
    arr[1] = 5;

    for(int i = 2; i < 1000000; i++)
    {
        arr[i] = ( (i+1) + arr[i-1] + (i+1)*arr[i-1]) % 1000000007;

    }


    for(int i = 0; i < queries; i++)
    {
        int temp;
        cin >> temp;
        cout << arr[temp-1]  << endl;
    }

    return 0;
}
