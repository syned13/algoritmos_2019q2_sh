#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <queue>

using namespace std;

typedef unsigned long long ll;




int main() {

    int x,y, z;
    cin >> x >> y >> z;

    int upVote = 0;
    int downVote = z;

    unordered_map<char,int> freq;



    while(downVote >= 0)
    {

        if(x + upVote > y + downVote)
            freq['+']++;
        else if(x + upVote < y + downVote)
            freq['-']++;
        else
            freq['0']++;

        upVote++;
        downVote--;
    }


    if(freq.size() > 1)
        cout << "?";
    else
    {
        int downs = freq['-'];
        int ups = freq['+'];
        int equal = freq['0'];

        if(downs > ups && downs > equal)
            cout << "-";
        else if(ups > equal && ups > downs)
            cout << '+';
        else
            cout << 0;
    }



    return 0;


}
