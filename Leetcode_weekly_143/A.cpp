class Solution {
public:
    vector<int> distributeCandies(int candies, int num_people) {

    vector<int> people(num_people);

    int turn = 0;
    while(candies > 0)
    {
        for(int i = 0; i < num_people; i++)
        {
            int aRestar = (turn*num_people) + i+1;
            //cout <<  candies << " - " << aRestar << " = " << candies - aRestar << endl;

            if(candies - aRestar >= 0)
            {
                people[i] += (turn*num_people) + i+1;
                candies -= (turn*num_people) + i+1;
            }
            else
            {
                people[i] += candies;
                candies = 0;
                break;
            }

            if(candies == 0)
                break;
        }
        turn++;

    }

    return people;


}
};
